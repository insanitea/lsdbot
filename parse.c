#include "lsdbot.h"

#include <clip/string.h>
#include <ctype.h>
#include <time.h>

int irc_parse_flags (char *str, char **name, bool *op, bool *voice) {
    if(*str == '@') {
        *op = true;
        *voice = true;
        ++str;
    }
    else if(*str == '+') {
        *op = false;
        *voice = true;
        ++str;
    }
    else {
        *op = false;
        *voice = false;
    }

    *name = strdup(str);
    if(*name == NULL)
        return -1;

    return 0;
}

int irc_parse_mask (char *mask, char **nick, char **ident, char **host) {
    int e;
    char *ptr,
         *str,
         *sp,
          sc;

    ptr = mask;
    sp = NULL;
    sc = 0;

//  extract nick
    str = strtoken(ptr, "!", &ptr, &sp, &sc);
    if(str == NULL) {
        e = 1;
        goto e1;
    }

    if(nick != NULL) {
        *nick = strdup(str);
        if(*nick == NULL) {
            e = -1;
            goto e1;
        }
    }

//  extract ident
    str = strtoken(ptr, "@", &ptr, &sp, &sc);
    if(str == NULL) {
        e = 1;
        goto e2;
    }

    if(ident != NULL) {
        *ident = strdup(str);
        if(*ident == NULL) {
            e = -1;
            goto e2;
        }
    }

//  restore string, extract leftover as host
    str = strtoken(ptr, NULL, NULL, &sp, &sc);
    if(str == NULL) {
        e = 1;
        goto e3;
    }

    if(host != NULL) {
        *host = strdup(str);
        if(*host == NULL) {
            e = -1;
            goto e3;
        }
    }

    return 0;

e3: if(ident != NULL)
        free(*ident);
e2: if(nick != NULL)
        free(*nick);

e1: return e;
}

int irc_parse_line (struct irc_event *ev, struct irc_conn *c, char *line) {
    int e;
    char *ptr,
         *sp,
          sc,
         *str;
    bool has_message;
    char **argv;
    size_t argc;

//  zero event struct
    memset(ev, 0, sizeof(struct irc_event));

//  set connection and time first
    ev->conn = c;
    time(&ev->time);

    e = 0;
    ptr = line;
    sp = NULL;
    sc = 0;

//  prefix
    if(*ptr == ':') {
        ++ptr;

        str = strtoken(ptr, " ", &ptr, &sp, &sc);
        if(str == NULL)
            goto error;

        e = irc_parse_mask(str, &ev->nick, &ev->ident, &ev->host);
        if(e == -1)
            goto error;

        if (e == 1) {
            ev->host = strdup(str);
            if (ev->host == NULL) {
                e = -1;
                goto error;
            }
        }
    }

//  extract arguments string
    str = strtoken(ptr, " :", &ptr, &sp, &sc);
    if(str == NULL) {
        str = strtoken(ptr, "\r\n", &ptr, &sp, &sc);
        if(str == NULL) {
            e = 1;
            goto error;
        }

        has_message = false;
    }
    else {
        has_message = true;
    }

//  split arguments string
    ev->argv = strsplit(str, " ", &ev->argc);
    if(ev->argv == NULL) {
        e = -1;
        goto error;
    }

//  extract message if there is one
    if(has_message) {
        str = strtoken(ptr, "\r\n", &ptr, &sp, &sc);
        if(str == NULL) {
            e = 1;
            goto error;
        }

        ev->message = strdup(str);
        if(ev->message == NULL) {
            e = -1;
            goto error;
        }
    }

    ev->action = ev->argv[0];
    if(ev->action == NULL) {
        e = -1;
        goto error;
    }

    if(strcmp(ev->action, "PRIVMSG") == 0) {
        if(ev->argc < 2) {
            e = 2;
            goto error;
        }

        if(ev->argv[1][0] == '#')
            ev->channel = ev->argv[1];
        else
            ev->target_nick = ev->argv[1];
    }
    else if(strcmp(ev->action, "JOIN") == 0 ||
            strcmp(ev->action, "PART") == 0 ||
            strcmp(ev->action, "TOPIC") == 0)
    {
        if(ev->argc < 2) {
            e = 2;
            goto error;
        }

        ev->channel = ev->argv[1];
    }
    else if(strcmp(ev->action, "KICK") == 0) {
        if(argc < 3) {
            e = 2;
            goto error;
        }

        ev->channel = ev->argv[1];
        ev->target_nick = ev->argv[2];
    }
    else if(strcmp(ev->action, "MODE") == 0) {
        if(ev->argc < 3) {
            e = 2;
            goto error;
        }

        ev->channel = ev->argv[1];
        ev->mode_changes = ev->argv[2];

        if(ev->argc > 3) {
            ev->mode_targets = ev->argv + 3;
            ev->n_mode_targets = ev->argc - 3;
        }
    }
    else if(strcmp(ev->action, "NICK") == 0) {
        if(ev->argc < 2) {
            e = 2;
            goto error;
        }

        ev->new_nick = ev->argv[1];
    }

    if(ev->nick != NULL) {
        ev->user = irc_conn_get_user(ev->conn, ev->nick);
        if(ev->user != NULL && ev->channel != NULL)
            ev->user_flags = irc_user_get_flags(ev->user, ev->channel);
    }

    if(ev->target_nick != NULL) {
        ev->target_user = irc_conn_get_user(ev->conn, ev->target_nick);
        if(ev->target_user != NULL && ev->channel != NULL)
            ev->target_user_flags = irc_user_get_flags(ev->target_user, ev->channel);
    }

    return 0;

error:
    irc_event_clear(ev);
    strtoken(NULL, NULL, NULL, &sp, &sc);

    return e;
}
