#include "lsdbot.h"

#include <dlfcn.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int irc_module_compile (char *name) {
    char *argv[3];
    int e,
        pid,
        stat;

    argv[0] = "./mkmod";
    argv[1] = name;
    argv[2] = NULL;

    pid = fork();
    if(pid == -1) {
        debug("%s: fork() failed\n", name);
        exit(1);
    }

    if(pid == 0 && execv(argv[0], argv) == -1)
        exit(1);

    if(waitpid(pid, &stat, 0) == -1) {
        debug("%s: waitpid() failed\n", name);
        return -1;
    }

    if(WIFEXITED(stat) == false) {
        e = -1;
        goto error;
    }

    e = WEXITSTATUS(stat);
    if(e != 0)
        goto error;

    return 0;

error:

    debug("%s: failed to compile\n", name);
    return e;
}

int irc_module_new (struct irc_bot *bot, char *name, struct irc_module **m_ret) {
    int e;
    struct irc_module *m;

//  allocate struct
    m = malloc(sizeof(struct irc_module));
    if(m == NULL)
        return -1;

    snprintf(m->name, sizeof(m->name), "%s", name);
    snprintf(m->lib_path, sizeof(m->lib_path), "modules/%s/%s.so", m->name, m->name);
    snprintf(m->config_path, sizeof(m->config_path), "modules/%s/config.json", m->name);

//  store struct
    e = map_insert(&bot->modules, m->name, m, free);
    if(e != 0) {
        free(m);
        return e;
    }

    *m_ret = m;
    return 0;
}

int irc_module_init (struct irc_bot *bot, struct irc_module *m) {
    int e;
    FILE *fh;

//  open shared library
    m->handle = dlopen(m->lib_path, RTLD_NOW);
    if(m->handle == NULL) {
        debug("%s: failed to open shared library\n", m->name);
        return -1;
    }

//  load configuration
    fh = fopen(m->config_path, "r");
    if(fh != NULL) {
        e = tree_read(&m->config, fh);
        fclose(fh);
        if(e != 0) {
            debug("%s: failed to read configuration [%i]\n", m->name, e);
            goto e1;
        }
    }

//  create root config node if it doesn't exist
    if(m->config == NULL) {
        m->config = tree_node_new(TREE_NODE_DICT, NULL, NULL);
        if(m->config == NULL) {
            e = -1;
            goto e1;
        }
    }

//  get init/deinit functions
    m->init = dlsym(m->handle, "init");
    m->deinit = dlsym(m->handle, "deinit");

    if(m->init != NULL) {
        e = m->init(bot, m->config);
        if(e != 0) {
            debug("%s: failed to initialize [%i]\n", m->name, e);
            goto e2;
        }
    }

    return 0;

e2: tree_node_destroy(m->config);
e1: dlclose(m->handle);

    return e;
}

int irc_module_deinit (struct irc_bot *bot, struct irc_module *m) {
    int e;
    FILE *fh;

    if(m->deinit != NULL) {
        e = m->deinit(bot, m->config);
        if(e != 0)
            debug("%s: failed to deinitialize [%i]\n", m->name, e);
    }
    else {
        e = 0;
    }

    if(m->config->child != NULL) {
        fh = fopen(m->config_path, "w+");
        if(fh != NULL) {
            tree_write(m->config, "    ", fh);
            fclose(fh);
        }
        else {
            debug("%s: failed to save configuration\n", m->name);
            e = -1;
        }
    }

    dlclose(m->handle);
    tree_node_destroy(m->config);

    m->handle = NULL;
    m->config = NULL;
    m->init = NULL;
    m->deinit = NULL;

    return e;
}
