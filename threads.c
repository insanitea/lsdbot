#include "lsdbot.h"

#include <clip/string.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/file.h>
#include <sys/inotify.h>
#include <sys/socket.h>
#include <unistd.h>

void * conn_thread (void *arg) {
    int e;
    struct irc_conn *c;
    struct irc_event ev;

    c = arg;

    if(c->fd == -1) {
        e = irc_conn_open(c);
        if(e != 0) {
            debug("error connecting to \"%s\" (%i)\n", c->name, e);
            return NULL;
        }

        debug("connected to \"%s\"\n", c->name);
    }
    else {
        debug("reloaded connection to \"%s\"\n", c->name);
    }

    while(c->fd != -1) {
        irc_bot_lock(c->bot);
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

        irc_conn_iter(c, &ev);
        irc_event_clear(&ev);

        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
        irc_bot_unlock(c->bot);
    }

    return NULL;
}

void * timer_thread (void *arg) {
    struct irc_bot *bot;
    size_t i;
    struct irc_timer *t;
    time_t now;

    bot = arg;

    while(bot->active) {
        sleep(1);

        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

        list_for_each(&bot->timers, i,t) {
            if(t->active == false)
                continue;

            now = time(NULL);
            if(now >= t->cutoff) {
                irc_bot_lock(bot);

                if(t->callback(t, t->data) == 1)
                    irc_timer_destroy(t);
                else
                    t->cutoff = now + t->timeout;

                irc_bot_unlock(bot);
            }
        }

        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    }

    return NULL;
};
