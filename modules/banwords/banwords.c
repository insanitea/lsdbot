#include <lsdbot.h>
#include <clip/string.h>
#include <string.h>

struct list banned_words;
char *default_msg;

struct word_rule {
    struct irc_user *user;
    char *word,
         *msg;
};

int make_word_rule (struct irc_conn *c, struct word_rule **r_ret, char *word, char *nick, char *msg) {
    struct word_rule *r;

    if(word == NULL || strlen(word) == 0)
        return 1;

    r = malloc(sizeof(struct word_rule));
    if(r == NULL)
        return -1;

    r->word = strdup(word);
    if(r->word == NULL) {
        free(r);
        return -1;
    }

    if(msg != NULL && strlen(msg) > 0) {
        r->msg = strdup(msg);
        if(r->msg == NULL) {
            free(r->word);
            free(r);
            return -1;
        }
    }
    else {
        r->msg = NULL;
    }

    if(nick != NULL && strlen(nick) > 0) {
        if(strcmp(nick, c->nick) == 0) {
            free(r->msg);
            free(r->word);
            free(r);
            return 2;
        }

        r->user = irc_conn_get_user(c, nick);
        if(r->user == NULL) {
            free(r->msg);
            free(r->word);
            free(r);
            return 3;
        }
    }
    else {
        r->user = NULL;
    }

    *r_ret = r;
    return 0;
}

void free_word_rule (struct word_rule *r) {
    free(r->msg);
    free(r->word);
    free(r);
}

lsdbot_listener(banwords) {
    struct irc_user *self;
    uint8_t *self_flags;
    size_t i;
    struct word_rule *r;
    char *msg;

    if(ev->channel == NULL)
        return 1;

    self = irc_conn_get_user(ev->conn, ev->conn->nick);
    if(self == NULL)
        return 0;

    self_flags = irc_user_get_flags(self, ev->channel);
    if(self_flags == NULL)
        return 1;

    list_for_each(&banned_words, i,r) {
        if((r->user == NULL || r->user == ev->user) &&
            strcasestr(ev->message, r->word) != NULL)
        {
            msg = r->msg != NULL ? r->msg : default_msg;

            if(*self_flags & FLAG_OP)
                irc_kick(ev->conn, ev->channel, ev->nick, msg);
            else if(msg != NULL)
                irc_reply(ev, "%s", msg);

            break;
        }
    }

    return 0;
}

lsdbot_command(cmd_ban_words) {
    int e;
    size_t n,i;
    struct word_rule *r;
    char *kick_msg;
    char **rulev;
    size_t rulec;

    n = 0;
    for(i = 0; i < argc; ++i) {
        rulev = strsplit(argv[i], ":", &rulec);
        if(rulev == NULL)
            break;
        else if(rulec > 3)
            continue;

        e = make_word_rule(
            ev->conn, rulev[0],
            rulec > 1 ? rulev[1] : NULL,
            rulec > 2 ? rulev[2] : NULL
        );

        strbuf_free(rulev, rulec);

        if(e == 2) {
            irc_reply(ev, "Nice try, smartass.");
            continue;
        }
        else if(e == 1) {
            continue;
        }
        else if(e == -1) {
            break;
        }

        e = list_append(&banned_words, r, free_word_rule);
        if(e != 0) {
            free_word_rule(r);
            break;
        }

        ++n;
        debug("added word rule [%s, %s, %s]\n", r->word, r->user != NULL ? r->user->nick : "<null>", r->msg != NULL ? r->msg : "<null>");
    }

    irc_reply(ev, "Added %zu %s.", n, n == 1 ? "rule" : "banned_words");
    return 0;
}

lsdbot_command(cmd_unban_words) {
    size_t i,j,k;
    char **rulev;
    size_t rulec;
    struct word_rule *r;

    for(i = 0, j = 0; i < argc; ++i) {
        rulev = strsplit(argv[i], ":", &rulec);
        if(rulev == NULL)
            return -1;

        list_for_each(&banned_words, k,r) {
            if(strcmp(rulev[0], r->word) == 0 &&
               ((rulec == 1 && r->user == NULL) ||
                (rulec == 2 && r->user != NULL &&
                 strcmp(r->user->nick, rulev[1]) == 0)))
            {
                list_remove(&banned_words, k--);
                ++j;
            }
        }

        strbuf_free(rulev, rulec);
    }

    irc_reply(ev, "Removed %zu %s", j, j == 1 ? "rule" : "banned_words");
    return 0;
}

lsdbot_module_func(init) {
    struct tree_node *rln,
                     *rn;

    list_init(&banned_words, 8, 8);

    tree_get(config, "default_msg", TREE_NODE_STR, &default_msg);
    if(default_msg == NULL)
        default_msg = strdup("No.");

    if(tree_descend(config, "banned_words", TREE_NODE_LIST, false, &rln) == 0) {
        tree_node_for_each(rln, rn) {
            list_append();
        }
    }

    irc_bot_add_command(bot, "banwords", 6, cmd_ban_words, 1, -1, -1);
    irc_bot_add_command(bot, "unbanwords", 6, cmd_unban_words, 1, -1, -1);
    irc_bot_add_listener(bot, NULL, "PRIVMSG", banwords, false);

    return 0;
}

lsdbot_module_func(deinit) {
    size_t i;
    struct word_rule *r;
    struct tree_node *rln,
                     *rn;

//  store banned_words

    tree_set(config, "default_msg", TREE_NODE_STR, &default_msg);

    rln = tree_node_new(TREE_NODE_LIST, "banned_words", config);
    list_for_each(&banned_words, i,r) {
        if(r->user != NULL && r->user->name == NULL)
            continue;

        rn = tree_node_new(TREE_NODE_DICT, NULL, rln);
        if(rn == NULL)
            break;

        if(r->user != NULL)
            tree_set(rn, "user", TREE_NODE_STR, r->user->name);

        tree_set(rn, "word", TREE_NODE_STR, &r->word);
        tree_set(rn, "msg", TREE_NODE_STR, &r->msg);
    }

    list_deinit(&banned_words);

    irc_bot_rm_command(bot, "banwords");
    irc_bot_rm_command(bot, "unbanwords");
    irc_bot_rm_listener(bot, NULL, "PRIVMSG", banwords);

    return 0;
}
