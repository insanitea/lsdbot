#include <lsdbot.h>

lsdbot_command(cmd_choice) {
    if (argc == 1)
        return irc_action(ev->conn, ev->channel, "sighs");
    else
        return irc_reply(ev, "%s", argv[lsdbot_rand() % argc]);
}

lsdbot_module_func(init) {
    return irc_bot_add_command(bot, "choice", 0, cmd_choice, 2, -1, -1);
}

lsdbot_module_func(deinit) {
    return irc_bot_rm_command(bot, "choice");
}
