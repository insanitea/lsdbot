#include <lsdbot.h>
#include <string.h>

struct list triggers;

char notas_is_buf[256],
     notas_its_buf[256],
     notas_thats_buf[256],
     notas_iam_buf[256],
     notas_im_buf[256],
     notas_are_buf[256],
     notas_youre_buf[256];

// TODO:
//
// put these in the default config you lazy fuck
//
// "dank", "cool", "awesome", "watty",
// "trippy", "high", "rekt", "fukt", "fucked", "fucky", "stoned", "drunk", "intoxicated",
// "cute", "sexy", "hot", "hawt", "good looking", "attractive", "handsome", "lovely", "beautiful"

lsdbot_command(notas_watch) {
    int e;
    size_t i;
    char *str;

    for(i = 0; i < ev->argc; ++i) {
        str = strdup(ev->argv[i]);
        if(str == NULL)
            return -1;

        e = list_append(&triggers, ev->argv[i], free);
        if(e != 0) {
            free(str);
            return -1;
        }
    }

    return 0;
}

lsdbot_listener(on_privmsg) {
    unsigned int r;
    size_t i;
    char *t;
    struct irc_user *u;

    r = lsdbot_rand() % 100;
    if(r > 30)
        return 1;

    if(ev->conn->users.used_size > 0) {
        list_for_each(&triggers, i,t) {
            snprintf(notas_is_buf,    sizeof(notas_is_buf),    "is %s",     t);
            snprintf(notas_its_buf,   sizeof(notas_its_buf),   "it's %s",   t);
            snprintf(notas_thats_buf, sizeof(notas_thats_buf), "that's %s", t);
            snprintf(notas_iam_buf,   sizeof(notas_iam_buf),   "i am %s",   t);
            snprintf(notas_im_buf,    sizeof(notas_im_buf),    "i'm %s",    t);
            snprintf(notas_are_buf,   sizeof(notas_are_buf),   "are %s",    t);
            snprintf(notas_youre_buf, sizeof(notas_youre_buf), "you're %s", t);

            if(strcasestr(ev->message, notas_is_buf)    != NULL ||
               strcasestr(ev->message, notas_its_buf)   != NULL ||
               strcasestr(ev->message, notas_thats_buf) != NULL ||
               strcasestr(ev->message, notas_iam_buf)   != NULL ||
               strcasestr(ev->message, notas_im_buf)    != NULL ||
               strcasestr(ev->message, notas_are_buf)   != NULL ||
               strcasestr(ev->message, notas_youre_buf) != NULL)
            {
                r = lsdbot_rand() % ev->conn->users.used_size;
                u = list_get(&ev->conn->users, r);
                irc_reply(ev, "Not as %s as %s.", t, u->nick);

                break;
            }
        }
    }

    return 0;
}

lsdbot_module_func(init) {
    int e;
    struct tree_node *n,
                     *t;
    char *trigger;

    e = tree_descend(config, "triggers", TREE_NODE_LIST, false, &n);
    if(e != 0)
        goto cleanup;

    tree_node_for_each(n,t) {
        if(t->type != TREE_NODE_STR ||
           t->data.str == NULL ||
           strlen(t->data.str) == 0)
        {
            continue;
        }

        trigger = strdup(t->data.str);
        if(trigger == NULL)
            return -1;

        e = list_append(&triggers, trigger, free);
        if(e != 0)
            return e;
    }

cleanup:
    return list_init(&triggers, 8, 8);
}

lsdbot_module_func(deinit) {
    int e;
    struct tree_node *n,
                     *t;
    size_t i;
    char *trigger;

    e = tree_descend(config, "triggers", TREE_NODE_LIST, false, &n);
    list_for_each(&triggers, i, trigger) {
        t = tree_node_new(TREE_NODE_STR, NULL, n);
        if(t == NULL) {
            e = -1;
            break;
        }

        tree_node_set_value(t, trigger);
    }

    return list_deinit(&triggers);
}
