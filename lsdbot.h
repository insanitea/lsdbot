#ifndef LSDBOT_H
#define LSDBOT_H

#define _GNU_SOURCE

#include <clip/list.h>
#include <clip/map.h>
#include <clip/tree.h>
#include <limits.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h> // for irc_replyf()
#include <stdint.h>
#include <time.h>

#define debug(...) fprintf(stderr, __VA_ARGS__)
#define lsdbot_rand() pcg32_random(&lsdbot_rng)

// RNG global
extern struct pcg32_rng lsdbot_rng;

// PCG32 / (C) 2014 M.E. O'Neill / pcg-random.org
// Apache License 2.0
struct pcg32_rng { uint64_t state, inc; };
uint32_t pcg32_random (struct pcg32_rng *r);
// ----

enum {
    EGETADDRINFO = 1,
    ESOCKET,
    ECONNECT,
    ESETSOCKOPT,
    EMODNULL,
    EMODOPEN,
    EMODINIT,
    EMODUNLOAD
};

enum {
    DATA_INT,
    DATA_UINT,
    DATA_FLOAT,
    DATA_STR
};

struct irc_bot;
struct irc_conn;
struct irc_channel;
struct irc_event;
struct irc_module;
struct irc_timer;
struct irc_user;

// bot.c

#define DEFAULT_CMD_PREFIX ";"

#define lsdbot_module_func(name) int name (struct irc_bot *bot, struct tree_node *config)
#define lsdbot_listener(name)    int name (struct irc_event *ev)
#define lsdbot_command(name)     int name (struct irc_event *ev, char **argv, size_t argc)

typedef int (irc_module_func)       (struct irc_bot *, struct tree_node *);
typedef int (irc_listener_callback) (struct irc_event *);
typedef int (irc_command_callback)  (struct irc_event *, char **, size_t);

struct irc_bot {
    pthread_mutex_t lock;
    pthread_t tm_thr;
    bool active;
    struct {
        char *nick,
             *user,
             *real_name,
             *quit_msg;
    } defaults;
    char *cmd_prefix;
    size_t cmd_prefix_len;
    time_t cmd_throttle;
    struct map commands,
               conns,
               modules;
    struct list listeners,
                timers;
};

struct irc_command {
    char *tag;
    unsigned int access;
    irc_command_callback *callback;
    size_t min_argc;
    ssize_t max_argc,
            max_split;
    char *syntax;
};

struct irc_listener {
    char *tag;
    struct irc_conn *conn;
    char *action;
    irc_listener_callback *callback;
    bool tmp;
};

struct irc_module {
    char name[NAME_MAX + 1],
         lib_path[PATH_MAX],
         config_path[PATH_MAX];

    void *handle;
    irc_module_func *init,
                    *deinit;
    struct tree_node *config;
};

#define irc_bot_clear_listeners(bot)  list_clear(&(bot)->listeners)
#define irc_bot_rm_command(bot,name)  map_remove(&(bot)->commands, name)
#define irc_bot_get_command(bot,name) map_get(&(bot)->commands, name)
#define irc_bot_get_conn(bot,name)    map_get(&(bot)->conns, name)
#define irc_bot_lock(bot)             pthread_mutex_lock(&(bot)->lock)
#define irc_bot_unlock(bot)           pthread_mutex_unlock(&(bot)->lock)

int  irc_bot_init             (struct irc_bot *bot);
int  irc_bot_save_config      (struct irc_bot *bot, char *path);
int  irc_bot_load_config      (struct irc_bot *bot, char *path);
int  irc_bot_deinit           (struct irc_bot *bot);
int  irc_bot_set_cmd_prefix   (struct irc_bot *bot, char *prefix);
void irc_bot_set_cmd_throttle (struct irc_bot *bot, time_t throttle);
int  irc_bot_set_defaults     (struct irc_bot *bot, char *nick, char *user, char *realname, char *quit_msg);
int  irc_bot_add_command      (struct irc_bot *bot, char *tag, char *name, unsigned int access, irc_command_callback *callback, size_t min_argc, ssize_t max_argc, ssize_t max_split);
int  irc_bot_add_listener     (struct irc_bot *bot, char *tag, struct irc_conn *c, char *action, irc_listener_callback *callback, bool tmp);
int  irc_bot_rm_listener      (struct irc_bot *bot, char *tag, struct irc_conn *c, char *action, irc_listener_callback *callback);
int  irc_bot_add_conn         (struct irc_conn **c_ret, struct irc_bot *bot, char *name, char *server, int port, char *password, char *nick, char *user, char *real_name, char *quit_msg, char *nickserv, char *nickserv_password);
int  irc_bot_rm_conn          (struct irc_bot *bot, char *name);
int  irc_bot_start            (struct irc_bot *bot);
void irc_bot_wait             (struct irc_bot *bot);
void irc_bot_stop             (struct irc_bot *bot);

// core.c

struct irc_command_tpl {
    char *name;
    int access;
    irc_command_callback *callback;
    size_t min_argc;
    ssize_t max_argc,
            max_split;
};

struct irc_listener_tpl {
    char *action;
    irc_listener_callback *callback;
};

extern struct irc_command_tpl lsdbot_core_commands[];
extern size_t lsdbot_core_commands_len;

extern struct irc_listener_tpl lsdbot_core_listeners[];
extern size_t lsdbot_core_listeners_len;

// client.c

int irc_reply    (struct irc_event *ev, char *format, ...);
int irc_discover (struct irc_conn *c, char *nick);
int irc_identify (struct irc_conn *c, char *password);

int irc_password (struct irc_conn *c, char *password);
int irc_nick     (struct irc_conn *c, char *nick);
int irc_user     (struct irc_conn *c, char *user, bool invisible);
int irc_oper     (struct irc_conn *c, char *nick, char *password);
int irc_mode     (struct irc_conn *c, char *channel, char *modes, char *args);
int irc_quit     (struct irc_conn *c, char *msg);
int irc_join     (struct irc_conn *c, char *channels, char *keys);
int irc_part     (struct irc_conn *c, char *channel, char *reason);
int irc_names    (struct irc_conn *c, char *channel);
int irc_kick     (struct irc_conn *c, char *channel, char *nick, char *msg);
int irc_privmsg  (struct irc_conn *c, char *target, char *msg);
int irc_privmsgf (struct irc_conn *c, char *target, char *format, ...);
int irc_action   (struct irc_conn *c, char *target, char *msg);
int irc_actionf  (struct irc_conn *c, char *target, char *format, ...);
int irc_notice   (struct irc_conn *c, char *target, char *notice);
int irc_who      (struct irc_conn *c, char *mask, bool ops);
int irc_whois    (struct irc_conn *c, char *nick);
int irc_pong     (struct irc_conn *c, char *msg);

// connection.c

struct irc_conn {
    pthread_t thr;
    struct irc_bot *bot;

    int fd,
        port;

    char *name,
         *host,
         *password,
         *nick,
         *user,
         *real_name,
         *part_msg,
         *quit_msg,
         *nickserv,
         *nickserv_password;

    struct map access;
    struct list users,
                channels;

    char rbuf[513],
         wbuf[513];
};

struct irc_channel {
    char *name,
         *key;
};

#define irc_conn_acl_rm(c, user) map_remove(&c->access, user)
#define irc_conn_sync(c,e,b,ev,cond) \
    while(true) { \
        e = irc_conn_iter(c,ev); \
        if(e != 0) \
            break; \
        b = cond; \
        irc_event_clear(ev); \
        if(b) \
            break; \
    }

int  irc_conn_new         (struct irc_conn **c, char *name, struct irc_bot *bot);
void irc_conn_destroy     (struct irc_conn *c);
int  irc_conn_init        (struct irc_conn *c, char *name, struct irc_bot *bot);
void irc_conn_deinit      (struct irc_conn *c);
int  irc_conn_configure   (struct irc_conn *c, char *server, int port, char *password, char *nick, char *user, char *real_name, char *quit_msg, char *nickserv, char *nickserv_password);
int  irc_conn_acl_add     (struct irc_conn *c, char *user, unsigned int level);
int  irc_conn_add_user    (struct irc_conn *c, char *name, char *nick, char *host, struct irc_user **u_ret);
int  irc_conn_rm_user     (struct irc_conn *c, char *name);
struct irc_user *
     irc_conn_get_user    (struct irc_conn *c, char *name);
int  irc_conn_add_channel (struct irc_conn *c, char *name, char *key, struct irc_channel **ch_ret);
int  irc_conn_rm_channel  (struct irc_conn *c, char *name);
struct irc_channel *
     irc_conn_get_channel (struct irc_conn *c, char *name);
int  irc_conn_open        (struct irc_conn *c);
int  irc_conn_close       (struct irc_conn *c);
int  irc_conn_reset       (struct irc_conn *c);
int  irc_conn_read_line   (struct irc_conn *c);
int  irc_conn_send_line   (struct irc_conn *c);
int  irc_conn_iter        (struct irc_conn *c, struct irc_event *ev);

// event.c

struct irc_event {
    time_t time;
    struct irc_conn *conn;

//  :nick!~ident@host action [argv ..] :message
//  or
//  :host reply [argv ..] :message

    uint16_t reply;
    char *nick,
         *ident,
         *host,
         **argv,
             *action,        //  argv[0]
             *channel,       //  argv[1] | PRIVMSG, JOIN, PART, KICK, MODE
             *target_nick,   //  argv[2] | PRIVMSG, KICK
             *mode_changes,  //  argv[1]
             **mode_targets, // &argv[2] | MODE
             *new_nick,      //  argv[1] | NICK
         *message;

    size_t argc,
           n_mode_targets;   // argc - 3 | MODE

    struct irc_user *user,
                    *target_user;
    struct irc_user_flags *user_flags,
                          *target_user_flags;
};

void irc_event_clear   (struct irc_event *ev);
int  irc_event_process (struct irc_event *ev);

// module.c

int irc_module_compile (char *name);
int irc_module_new     (struct irc_bot *bot, char *name, struct irc_module **m_ret);
int irc_module_init    (struct irc_bot *bot, struct irc_module *m);
int irc_module_deinit  (struct irc_bot *bot, struct irc_module *m);

// parse.c

int irc_parse_flags (char *name, char **nick, bool *op, bool *voice);
int irc_parse_mask  (char *mask, char **nick, char **user, char **host);
int irc_parse_line  (struct irc_event *ev, struct irc_conn *c, char *line);

// timer.c

#define lsdbot_timer(name)  int name (struct irc_timer *t, void *data)
#define irc_timer_lock(t)   pthread_mutex_lock(&(t)->lock)
#define irc_timer_unlock(t) pthread_mutex_unlock(&(t)->lock)

typedef int (irc_timer_callback) (struct irc_timer *, void *);

struct irc_timer {
    bool active;
    struct irc_conn *conn;
    unsigned long long timeout;
    time_t cutoff;
    irc_timer_callback *callback;
    void *data;
};

struct irc_timer *
     irc_timer_new     (struct irc_conn *c, char *time_desc, irc_timer_callback *callback, void *data);
void irc_timer_destroy (struct irc_timer *t);
void irc_timer_start   (struct irc_timer *t);
void irc_timer_stop    (struct irc_timer *t);

// user.c

// ACCESS LEVELS:
//  -1: unknown
//   0: unprivileged
//   1: [reserved]
//   2: [reserved]
//   3: [reserved]
//   4: operator
//   5: administrator

enum {
    FLAG_OP =     0x01,
    FLAG_VOICE =  0x02,
    FLAG_QUIET =  0x04,
    FLAG_ALL    = 0x07
};

struct irc_user {
    char *name,
         *nick,
         *ident,
         *host;

    time_t last_cmd;
    int access;
    struct map data,
               flags;
};

#define irc_user_auth(u,a)       (u != NULL && u->access >= a)
#define irc_user_get_flags(u,ch) map_get(&u->flags, ch)

struct irc_user *
       irc_user_new          (char *nick, char *user, char *host);
void   irc_user_destroy      (struct irc_user *u);
char * irc_user_get_mask     (struct irc_user *u, bool nick, bool ident, bool host);
int    irc_user_set_data     (struct irc_user *u, int type, char *key, void *data);
int    irc_user_update       (struct irc_user *u, char *name, char *nick, char *host);
int    irc_user_update_flags (struct irc_user *u, char *channel, uint8_t mask, uint8_t flags);

#endif
