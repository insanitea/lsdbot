#include "lsdbot.h"

#include <string.h>

// replies to an event; prepends <nick>: if from a channel
int irc_reply (struct irc_event *ev, char *format, ...) {
    char preformat[513];
    va_list args;

    if(ev->channel != NULL)
        snprintf(preformat, sizeof(preformat), "PRIVMSG %s :%s: %s\r\n", ev->channel, ev->nick, format);
    else
        snprintf(preformat, sizeof(preformat), "PRIVMSG %s :%s\r\n", ev->nick, format);

    va_start(args, format);
    vsnprintf(ev->conn->wbuf, sizeof(ev->conn->wbuf), preformat, args);
    va_end(args);

    return irc_conn_send_line(ev->conn);
}

// user discovery
int irc_discover (struct irc_conn *c, char *nick) {
    int e;
    bool b;
    struct irc_event ev;

//  perform WHOIS
    e = irc_whois(c, nick);
    if(e != 0)
        return e;

//  wait for response to be processed
    irc_conn_sync(c, e, b, &ev,
                  strcmp(ev.action, "318") == 0 &&
                  ev.argc > 2 && strcmp(ev.argv[2], nick) == 0);

    return 0;
}

// identifies via nickserv
int irc_identify (struct irc_conn *c, char *password) {
    if(c->nickserv == NULL)
        return 1;

    snprintf(c->wbuf, sizeof(c->wbuf), "PRIVMSG %s :identify %s\r\n", c->nickserv, password);
    return irc_conn_send_line(c);
}

// ----

int irc_password (struct irc_conn *c, char *password) {
    snprintf(c->wbuf, sizeof(c->wbuf), "PASS %s\r\n", password);
    return irc_conn_send_line(c);
}

int irc_nick (struct irc_conn *c, char *nick) {
    snprintf(c->wbuf, sizeof(c->wbuf), "NICK %s\r\n", nick);
    return irc_conn_send_line(c);
}

int irc_user (struct irc_conn *c, char *user, bool invisible) {
    snprintf(c->wbuf, sizeof(c->wbuf), "USER %s %c * :%s\r\n", user, invisible ? '8' : '0', c->real_name ? c->real_name : c->user);
    return irc_conn_send_line(c);
}

int irc_oper (struct irc_conn *c, char *nick, char *password) {
    snprintf(c->wbuf, sizeof(c->wbuf), "OPER %s %s\r\n", nick, password);
    return irc_conn_send_line(c);
}

int irc_mode (struct irc_conn *c, char *channel, char *modes, char *args) {
    if(args != NULL)
        snprintf(c->wbuf, sizeof(c->wbuf), "MODE %s %s %s\r\n", channel, modes, args);
    else
        snprintf(c->wbuf, sizeof(c->wbuf), "MODE %s %s\r\n", channel, modes);

    return irc_conn_send_line(c);
}

int irc_quit (struct irc_conn *c, char *msg) {
    if(msg != NULL)
        snprintf(c->wbuf, sizeof(c->wbuf), "QUIT :%s\r\n", msg);
    else
        snprintf(c->wbuf, sizeof(c->wbuf), "QUIT\r\n");

    return irc_conn_send_line(c);
}

int irc_join (struct irc_conn *c, char *channels, char *keys) {
    if(keys == NULL)
        snprintf(c->wbuf, sizeof(c->wbuf), "JOIN %s\r\n", channels);
    else
        snprintf(c->wbuf, sizeof(c->wbuf), "JOIN %s %s\r\n", channels, keys);

    return irc_conn_send_line(c);
}

int irc_part (struct irc_conn *c, char *channel, char *msg) {
    if(msg != NULL)
        snprintf(c->wbuf, sizeof(c->wbuf), "PART %s :%s\r\n", channel, msg);
    else
        snprintf(c->wbuf, sizeof(c->wbuf), "PART %s\r\n", channel);

    return irc_conn_send_line(c);
}

int irc_names (struct irc_conn *c, char *channel) {
    if(channel != NULL)
        snprintf(c->wbuf, sizeof(c->wbuf), "NAMES %s\r\n", channel);
    else
        snprintf(c->wbuf, sizeof(c->wbuf), "NAMES\r\n");

    return irc_conn_send_line(c);
}

int irc_kick (struct irc_conn *c, char *channel, char *nick, char *msg) {
    if(msg != NULL)
        snprintf(c->wbuf, sizeof(c->wbuf), "KICK %s %s :%s\r\n", channel, nick, msg);
    else
        snprintf(c->wbuf, sizeof(c->wbuf), "KICK %s %s\r\n", channel, nick);

    return irc_conn_send_line(c);
}

int irc_privmsg (struct irc_conn *c, char *target, char *msg) {
    snprintf(c->wbuf, sizeof(c->wbuf), "PRIVMSG %s :%s\r\n", target, msg);
    return irc_conn_send_line(c);
}

int irc_privmsgf (struct irc_conn *c, char *target, char *format, ...) {
    char preformat[513];
    va_list args;
    int e;

    snprintf(preformat, sizeof(preformat), "PRIVMSG %s :%s\r\n", target, format);
    va_start(args, format);
    vsnprintf(c->wbuf, sizeof(c->wbuf), preformat, args);
    e = irc_conn_send_line(c);
    va_end(args);

    return e;
}

int irc_action (struct irc_conn *c, char *target, char *msg) {
    snprintf(c->wbuf, sizeof(c->wbuf), "PRIVMSG %s :\001ACTION %s\001\r\n", target, msg);
    return irc_conn_send_line(c);
}

int irc_actionf (struct irc_conn *c, char *target, char *format, ...) {
    char preformat[513];
    va_list args;
    int e;

    snprintf(preformat, sizeof(preformat), "PRIVMSG %s :\001%s\001\r\n", target, format);
    va_start(args, format);
    vsnprintf(c->wbuf, sizeof(c->wbuf), preformat, args);
    e = irc_conn_send_line(c);
    va_end(args);

    return e;
}

int irc_notice (struct irc_conn *c, char *target, char *notice) {
    snprintf(c->wbuf, sizeof(c->wbuf), "NOTICE %s :%s\r\n", target, notice);
    return irc_conn_send_line(c);
}

int irc_who (struct irc_conn *c, char *mask, bool ops) {
    snprintf(c->wbuf, sizeof(c->wbuf), "WHO %s%s\r\n", mask, ops ? " o" : "");
    return irc_conn_send_line(c);
}

int irc_whois (struct irc_conn *c, char *mask) {
    snprintf(c->wbuf, sizeof(c->wbuf), "WHOIS %s\r\n", mask);
    return irc_conn_send_line(c);
}

int irc_pong (struct irc_conn *c, char *msg) {
    if(msg != NULL)
        snprintf(c->wbuf, sizeof(c->wbuf), "PONG :%s\r\n", msg);
    else
        snprintf(c->wbuf, sizeof(c->wbuf), "PONG\r\n");

    return irc_conn_send_line(c);
}
