#include "lsdbot.h"

struct pcg32_rng lsdbot_rng;

// PCG32 / (C) 2014 M.E. O'Neill / pcg-random.org
// Apache License 2.0

uint32_t pcg32_random (struct pcg32_rng *rng) {
    uint64_t old;
    uint32_t xs,
             rot;

    old = rng->state;
    xs = ((old >> 18u) ^ old) >> 27u;
    rot = old >> 59u;

    rng->state = old * 6364136223846793005ULL + (rng->inc|1);
    return (xs >> rot) | (xs << ((-rot) & 31));
}
